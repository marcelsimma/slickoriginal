package at.simma.projects.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLCircle implements Actor {
	private double diameter, y, speed;
	private double[] x = {0,0};

	public HTLCircle(double x,double y, double diameter, double speed) {
		this.diameter = diameter;
		this.x[0] = x;
		this.y = y;
		this.speed = speed;
	}
	
	public void update(GameContainer container, int delta) {
		if (this.x[0] >= 570) {
			this.x[1] = 1;
			this.x[0] -= speed;
		} else if (this.x[0] <= 10) {
			this.x[1] = 0;
			this.x[0] += speed;
		} else if (this.x[1] == 0) {
			this.x[0] += speed;
		} else {
			this.x[0] -= speed;
		}
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x[0], (float)this.y, (float)this.diameter, (float)this.diameter);
	}
}
