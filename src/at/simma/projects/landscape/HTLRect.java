package at.simma.projects.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLRect implements Actor{
	private double width, height, x, y, degree, speed, radius, centerX, centerY;

	public HTLRect(double centerX, double centerY, double width, double height, double speed, double degree, double radius) {
		this.width = width;
		this.height = height;
		this.degree = degree;
		this.centerX = centerX;
		this.centerY = centerY;
		this.speed = speed;
		this.radius = radius;
	}
	
	public void update(GameContainer container, int delta) {
		this.x = this.centerX + Math.cos(this.degree) * this.radius;
		this.y = this.centerY + Math.sin(this.degree) * this.radius;
		this.degree += speed;
	}
	
	public void render(Graphics graphics) {
		graphics.drawRect((float)this.x, (float)this.y, (float)this.width, (float)this.height);
	}
	
	
	
	
}
