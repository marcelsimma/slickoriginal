package at.simma.projects.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class Snowflake implements Actor {
	private double width, height, x, y, speed;

	public Snowflake(String groesse) {
		double size = 0;
		switch (groesse) {
		case "small":
			size = 8;
			break;
		case "middle":
			size = 15;
			break;
		case "large":
			size = 20;
			break;
		default:
			break;
		}
		this.width = size;
		this.height = size;
		this.speed = size / 100;
		this.setPos();
	}

	public void setPos() {
		this.x = Math.random() * 600;
		this.y = Math.random() * -600;
	}

	public void update(GameContainer gc, int delta) {
		this.y += speed * delta;
		if (y >= 600) {
			this.setPos();
		}
	}

	public void render(Graphics graphics) {
		graphics.fillOval((float) this.x, (float) this.y, (float) this.width, (float) this.height);
	}

}
