package at.simma.projects.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Canon implements Actor {
	private double width, height, x, y, degree, speed, radius, startY;

	public Canon(double x, double y, double width, double height, double speed, double degree, double radius) {
		this.width = width;
		this.height = height;
		this.degree = degree;
		this.x = x;
		this.startY = y;
		this.speed = speed;
		this.radius = radius;
		this.y = y;
	}

	public void update(GameContainer container, int delta) {
		this.x += speed;
		this.y = Math.pow(2.2, 1 + (this.x / 100)) + this.startY;
	}

	public void render(Graphics graphics) {
		graphics.fillRect((float) this.x, (float) this.y, (float) this.width, (float) this.height);
	}

}
