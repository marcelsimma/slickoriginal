package at.simma.projects.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLOval implements Actor{
	private double width, height, x, y, speed;

	public HTLOval(double x, double y, double  width, double height, double speed) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.speed = speed;
	}
	
	public void update(GameContainer container, int delta) {
		this.y += speed;
		if (y >= 620) {
			this.y = -20;
		}
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x, (float)this.y, (float)this.width, (float)this.height);
	}
}
