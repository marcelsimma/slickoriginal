package at.simma.projects.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Player implements Actor {
	private double width, height, x, y, speed, centerX, centerY;
	Landscape landscape;
	private int cooldown;

	public Player(double x, double y, double width, double height, double speed, Landscape landscape) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.landscape = landscape;
		this.cooldown = 0;
	}

	@Override
	public void update(GameContainer gc, int delta) {
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			this.y--;
		}
		if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			this.y++;
		}
		if (this.cooldown > 500) {
			if (gc.getInput().isKeyPressed(Input.KEY_SPACE)) {
				landscape.addActor(new Canon(this.x + this.width, this.y, 10, 10, 1, 10, 10));
				System.out.println("space");
			}
			cooldown = 0;
		} else {
			cooldown += delta;
		}
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawRect((float) this.x, (float) this.y, (float) this.width, (float) this.height);
	}

}
